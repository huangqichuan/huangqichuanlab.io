---
layout: post
title: "云平台配置手册-01"
date:   2017-05-02 21:47:22
categories: cloud
---

# 云平台配置手册 - 01 

本手册记录了以下内容：
- 节点基础网络配置
- 配置 SSH 免密码登录
- 安装 JDK、Scala
- 配置完全分布式的 Hadoop
- 配置完全分布式的 HBase
- 配置 StandAlone 模式的 Spark
- 安装和配置 MySQL
- 安装和配置 Hive
- 安装和配置 Sqoop


## 一、节点基础网络配置

- 硬件上使集群中的各个节点处于同一局域网内。
- 设置各个节点中的网卡自动连接到网络。
	1. 编辑`/etc/sysconfig/network-scripts/ifcfg-eth0`。
	2. 设置`ONBOOT=yes`。
	3. 保存退出。 
- 设置每台节点的 hostname 为不同名称。
	例如：
	```
	spark1
	spark2
	spark3
	```
- 根据实际网络环境，为每台节点设置固定的IP地址。
	例如：
	```
	spark1  192.168.88.51
	spark2  192.168.88.52
	spark3  192.168.88.53
	
	子网掩码 255.255.255.0
	网关地址 192.168.88.1
	```
- 在每台节点的 `/etc/hosts` 文件中添加解析。
	例如：
	```
	192.168.88.51	spark1  
	192.168.88.52	spark2
	192.168.88.53	spark3
	```
- 关闭每台节点的防火墙
	```
	sudo service iptables stop
	sudo chkconfig iptables off
	```
- 检查节点之间是否可以互相 ping 通。
	例如在 spark1 节点上：
	```
	ping spark2
	ping spark3
	```


## 二、配置 SSH 免密码登录
### 2.1 准备工作

- 保证每台节点中的用户名相同（例如都为`spark`）。如果没有，则创建。
> 不要直接使用 root 用户。请为集群单独创建用户。

### 2.2 将用户添加至 sudoer 列表

1. 切换到 root 用户（`su -`）。
2. 使用 `visudo` 命令，添加 spark 用户到 sudoer 列表。
	```
	# 在 root 用户之后添加（请根据实际情况赋予权限）
	root    ALL=(ALL)       ALL
	spark   ALL=(ALL)       NOPASSWD:ALL
	```
3. 保存退出。

### 2.3 配置 SSH
1. 编辑 `/etc/ssh/sshd-config` 文件（注意权限）。
	去除以下选项前的注释符`#`，使选项生效。
	```
	RSAAuthentication  yes
	PubkeyAuthentication  yes
	AuthorizedKeysFile .ssh/authorized_keys
	```
2. 修改完成后，重启 sshd 服务：`sudo service sshd restart` 。
3. 在 `/home/spark/.ssh` 目录下生成公钥、私钥：`ssh_keygen` 。
4. 将每个节点的 `id_rsa.pub` 文件互相发送至其他节点（`scp` 命令）。
5. 对于每一个节点，将所有的公钥写入 `authorized_keys` 文件（例如 `cat id_rsa.pub >> authorized_keys`）。
6. 修改目录和文件权限。
	```
	chomd 700 .ssh
	chomd 600 .ssh/authorized_keys
	```

### 2.4 如有必要，配置内网 NTP 时间同步服务并同步时间。

## 三、安装 JDK、Scala

### 3.1 安装 JDK

1. 下载 JDK 。例如 `jdk1.8.0_121.tar.gz`。
2. 解压到 `/opt` 目录下并设置目录权限。
3. 配置环境变量。
	- `sudo vim /etc/profile`。
	- 在文件中添加：
	```
	export JAVA_HOME=/opt/jdk1.8.0_121
	export PATH=$JAVA_HOME/bin:$PATH
	export CLASSPATH=.:$JAVA_HOME/lib/dt.jar:$JAVA_HOME/lib/tools.jar
	```
	> 配置完成后应使用 `source /etc/profile` 使其生效，并使用 `java -version` 命令检查安装是否成功。

### 3.2 安装 Scala

1. 下载 Scala 。例如 `scala-2-10-6.tgz`。
2. 解压到 `/opt` 目录下并设置目录权限。
3. 配置环境变量。
	- `sudo vim /etc/profile`。
	- 在文件中添加：
	```
	export SCALA_HOME=/opt/scala-2.10.6
	export PATH=$PATH:$SCALA_HOME/bin
	```
	> 配置完成后应使用 `source /etc/profile` 使其生效，并使用 `scala` 命令检查安装是否成功。

## 四、配置完全分布式的 Hadoop

### 4.1 配置 Hadoop

1. 下载 Hadoop 。例如 `hadoop-2.6.5.tar.gz` 。
2. 解压到 `/opt` 目录下并设置目录权限。
3. 配置环境变量。
	- `sudo vim /etc/profile`
	- 在文件中添加：
	```
	export HADOOP_HOME=/opt/hadoop-2.6.5
	export PATH=$PATH:$HADOOP_HOME/bin:$HADOOP_HOME/sbin
	```
4. 更改配置文件（位于 `/opt/hadoop-2.6.5/etc/hadoop` 目录）。
	- hadoop-env.sh
		找到并修改： `export JAVA_HOME=/opt/jdk1.8.0_121` 。
	- core-site.xml
		找到并修改：
		```
		<configuration>
			<property>
				<name>fs.defaultFS</name>
				<value>hdfs://spark1:9000</value>
			</property>
			<property>
				<name>hadoop.tmp.dir</name>
				<value>file:/home/spark/hadoop-2.6.5/tmp</value>
				<description>Abase for ohr temporary directories.</description>
			</property>
		</configuration>
		```
		> 请注意记录，此处的HDFS端口号为 `9000` 。
	- hdfs-site.xml  
		找到并修改：
		```
		<configuration>
			<property>
				<name>dfs.namenode.secondary.http-address</name>
				<value>spark1:50090</value>
			</property>
			<property>
				<name>dfs.replication</name>
				<value>2</value>
			</property>
			<property>
				<name>dfs.namenode.name.dir</name>
				<value>file:/home/spark/hadoop-2.6.5/tmp/dfs/name</value>
			</property>
			<property>
				<name>dfs.datanode.data.dir</name>
				<value>file:/home/spark/hadoop-2.6.5/data</value>
			</property>
		</configuration>
		```
		> 请根据实际情况修改 `dfs.replication` 的值（默认为 3 ）。
	- mapred-site.xml
		找到并修改：
		```
		<configuration>
			<property>
				<name>mapreduce.framework.name</name>
				<value>yarn</value>
			</property>
			<property>
				<name>mapreduce.jobhistory.address</name>
				<value>spark1:10020</value>
			</property>
			<property>
				<name>mapreduce.jobhistory.webapp.address</name>
				<value>spark1:19888</value>
			</property>
		</configuration>
		```
	- yarn-site.xml
		找到并修改：
		```
		<configuration>
			<property>
				<name>yarn.resourcemanager.hostname</name>
				<value>spark1</value>
			</property>
			<property>
				<name>yarn.nodemanager.aux-services</name>
				<value>mapreduce_shuffle</value>
			</property>
		</configuration>
		```
	- 复制 slaves.template 为 slaves，并编辑 slaves 文件。
		添加：
		```
		spark2
		spark3
		```
5. 将 Hadoop 整个目录复制到 spark2 和 spark3 节点（scp 命令）。

### 4.2 启动和关闭 Hadoop 服务

#### 4.2.1 启动 Hadoop 的步骤

1. **（仅首次启动前执行）**格式化 NameNode ：`hadoop namenode -format`。
2. 启动 HDFS 文件系统服务：`start-dfs.sh`。
3. 启动 YARN：`start-yarn.sh`。
4. 使用 `jps` 命令查看相关进程是否启动。
	spark1：
	- NameNode
	- SecondaryNameNode
	- ResourceManager
	spark2 和 spark3：
	- DataNode
	- NodeManager

> 如果上述进程未能全部启动，可以按照以下步骤排查：
> 1. 使用 `stop-yarn.sh` 和 `stop-dfs.sh` 关闭所有服务。
> 2. 检查 spark 用户是否拥有相关目录的权限。
> 3. 尝试清空 core-site.xml 配置文件中的 `hadoop.tmp.dir` 配置项所指向的目录。
 
#### 4.2.2 关闭 Hadoop 的步骤

依次输入 `stop-yarn.sh` 和 `stop-dfs.sh` 即可。

> **注意：每次关闭系统之前必须执行该步骤。**

## 五、配置完全分布式的 HBase

### 5.1 配置 HBase

1. 下载 HBase ，例如 `hbase-1.2.5.tar.gz` 。
2. 解压到 `/opt` 目录并设置目录权限。
3. 编辑 `habse-env.sh` （位于 `/opt/hbase-1.2.5/conf` 目录，下同）。
	找到或添加 `export JAVA_HOME=/opt/jdk1.8.0_121`
4. 编辑 `habse-site.xml` 。
	找到并修改：
	```
	<configuration>
		<property>
			<name>hbase.rootdir</name>
			<value>hdfs://spark1:9000/hbase</value>
		</property>
		<property>
			<name>hbase.cluster.distributed</name>
			<value>true</value>
		</property>
		<property>
			<name>hbase.zookeeper.quorum</name>
			<value>spark1,spark2,spark3</value>
		</property>
		<property>
			<name>hbase.zookeeper.property.dataDir</name>
			<value>/home/spark/zookeeper</value>
		</property>
	</configuration>
	```
	> 注意 `hbase.rootdir` 中的 HDFS 端口号为 9000 。
5. 编辑 `reginserver` 。
	删除原有的 `localhost`，添加：
	```
	spark2
	spark3
	```
6. 新建 backup-masters 文件，添加：
	```
	spark2
	```

### 5.2 启动和关闭 HBase 服务

#### 5.2.1 启动 HBase 的步骤

1. 执行 `start-hbase.sh` 。
2. 使用 `jps` 命令检查相关进程是否已经启动。
	spark1：
	- HMaster
	- HQuorumPeer
	spark2：
	- HMaster
	- HQuorumPeer
	- HRegionServer
	spark3：
	- HQuorumPeer
	- HRegionServer

#### 5.2.2 关闭 HBase 的步骤

执行 `stop-hbase.sh` 即可。

> **注意：每次关闭系统之前必须执行该步骤。**

## 六、配置 StandAlone 模式的 Spark

### 6.1 配置 Spark

1. 下载 Spark 。例如 `spark-1.6.1-bin-hadoop2.6.tgz` 。
2. 解压至 `/opt` 目录并设置目录权限。
3. 复制 spark-env.sh.template 为 spark-env.sh 文件（位于 `/opt/spark-1.6.1-bin-hadoop2.6/conf` 目录，下同）。在 spark-env.sh 文件中添加以下配置项：
	```
	export SCALA_HOME=/opt/scala-2.10.6
	export JAVA_HOME=/opt/jdk1.8.0_121
	export SPARK_MASTER_IP=192.168.88.51
	export SPARK_WORKER_INSTANCE=2
	export SPARK_MASTER_PORT=8070
	export SPARK_MASTER_WEBUI_PORT=8090
	export SPARK_WORKER_PORT=8092
	export SPARK_WORKER_MEMORY=3072m
	```
4. 复制 slaves.template 为 slaves 文件。在 slaves 文件中添加以下配置项：
	```
	spark2 #192.168.88.52
	spark3 #192.168.88.53
	```

### 6.2 启动和关闭 Spark 服务

#### 6.2.1 启动 Spark 服务

1. 依次执行 `start-master.sh` 和 `start-slaves.sh` 。
2. 执行 `jps` 命令查看相关进程是否已经启动。
	spark1：
	- Master
	spark2 和 spark3 ：
	- Worker

#### 6.2.2 关闭 Spark 服务

1. 依次执行 `stop-slaves.sh` 和 `stop-master.sh` 即可。

## 七、安装和配置 MySQL

### 7.1 安装 MySQL

1. 下载 MySQL 的 yum 仓库文件。
    ```
    cd ~/Downloads
    wget https://dev.mysql.com/get/mysql57-community-release-el6-10.noarch.rpm
    ```
2. 安装 MySQL yum 源。
	```
	sudo rpm -Uvh mysql57-community-release-el6-7.noarch.rpm
	```
3. 安装 MySQL Server。
	```
	sudo yum install mysql-community-server
	```
4. 安装完成后，测试启动 MySQL 服务。
	```
	sudo service mysqld start
	```

### 7.2 配置 MySQL 数据库
1. 停止 MySQL 数据库服务。
	```
	sudo service mysqld stop
	```
	> 或使用 `/etc/init.d/mysqld stop` 命令。
2. 使用安全模式启动 MySQL 服务，并继续监控其运行情况。
	```
	audo mysqld_safe --user=mysql --skip-grant-tables --skip-networking &
	```
3. 使用 root 用户登录 MySQL 数据库。
	```
	mysql -u root mysql
	```
4. 更新 root 用户密码。
	```
	mysql> UPDATE user SET authentication_string=PASSWORD('newpassword') where USER='root';
	```
	**重要：请牢记此处的 newpassword （根据实际情况修改，例如 root ）。**
	> 在 5.7 之前的版本中，请使用 `mysql> UPDATE user SET Password=PASSWORD('newpassword') where USER='root';`
5. 刷新权限。
	```
	mysql> FLUSH PRIVILEGES;
	```
6. 退出 MySQL 。
	```
	mysql> quit
	```
7. 重启 MySQL 服务。
	```
	sudo service mysqld restart
	```
	> 或使用 `/etc/init.d/mysqld restart` 命令。
8. 使用 root 用户重新登录 MySQL 。
	```
	mysql -u root -p 
	Enter password: <输入新设的密码newpassword>
	```
9. 开放 3306 端口访问权限。
	```
	mysql> USE mysql;   
	mysql> SELECT host,user FROM user;  
	mysql> UPDATE user SET host = '%' WHERE user ='root';  
	mysql> SELECT host,user FROM user;  
	mysql> FLUSH PRIVILEGES;
	mysql> QUIT;
	```
	退出后重启 MySQL 服务。
	```
	sudo service mysqld restart
	```
	使用 `netstat -an|grep 3306` 命令检查该端口是否开放。如果看到：
	```
	tcp        0      0 :::3306                     :::*                        LISTEN
	```
	则说明 3306 开放端口成功。

## 八、安装和配置 Hive

### 8.1 配置 Hive 

1. 下载 Hive 。例如 `apache-hive-2.1.1-bin.tar.gz`。
2. 解压至 `/opt` 目录并设置目录权限。
3. 用 vim 编辑器在 `/etc/profile` 中添加以下内容：
	```
	# Hive HOME
	export HIVE_HOME=/opt/apache-hive-2.1.1-bin
	export PATH=$PATH:$HIVE_HOME/bin:$HIVE_HOME/conf
	```
	> 不要忘记 `source /etc/profile` 使其生效。
4. 在 HDFS 中创建一些用于存放数据仓库和临时文件的目录。
	```
	hadoop fs -mkdir /tmp
	hadoop fs -mkdir /tmp/hive
	hadoop fs -mkdir /user
	hadoop fs -mkdir /user/hive
	hadoop fs -mkdir /user/hive/warehouse
	hadoop fs -chmod g+w /tmp
	hadoop fs -chmod g+w /user/hive/warehouse
	```
5. 将模板文件复制为配置文件。上述文件位于 `/opt/apache-hive-2.1.1-bin/conf` 目录。
	```
	cp hive-default.xml.template hive-site.xml
	cp hive-log4j2.properties.template hive-log4j2.properties
	cp hive-exec-log4j2.properties.template hive-exec-log4j2.properties
	cp hive-env.sh.template hive-env.sh
	```
6. 编辑 hive-site.xml ，下列属性配置项可以通过搜索找到。
	(1) 找到以下属性，修改 value 的值：
	
	```
	  <property>
	    <name>hive.exec.scratchdir</name>
	    <value>/tmp/hive</value>
	    <description>HDFS root scratch dir for Hive jobs which gets created with write all (733) permission. For each connecting user, an HDFS scratch dir: ${hive.exec.scratchdir}/&lt;username&gt; is created, with ${hive.scratch.dir.permission}.</description>
	  </property>
	```
	
	请确认此处的目录已在HDFS中创建。

	(2) 找到以下属性，修改 value 的值：
	
	```
	  <property>
	    <name>hive.metastore.warehouse.dir</name>
	    <value>/user/hive/warehouse</value>
	    <description>location of default database for the warehouse</description>
	  </property>
	```
	
	请确认此处的目录已在HDFS中创建。

	(3) 约第500行，找到以下属性：

	```
	  <property>
	    <name>javax.jdo.option.ConnectionURL</name>
	    <value>jdbc:derby:;databaseName=metastore_db;create=true</value>
	    <description>
	      JDBC connect string for a JDBC metastore.
	      To use SSL to encrypt/authenticate the connection, provide database-specific SSL flag in the connection URL.
	      For example, jdbc:postgresql://myhost/db?ssl=true for postgres database.
	    </description>
	  </property>
	```
	
	将 value 修改为：
	
	```
	<value>jdbc:mysql://192.168.88.51:3306/hive?createDatabaseIfNotExist=true</value>
	```
	
	(4) 约第 484 行，找到以下属性：
	
	```
	  <property>
	    <name>javax.jdo.option.ConnectionPassword</name>
	    <value>mine</value>
	    <description>password to use against metastore database</description>
	  </property>
	```
	
	将 value 修改为：
	
	```
	<value>mine</value>
	```
	
	(5) 约第 956 行，找到以下属性：
	
	```
	  <property>
	    <name>javax.jdo.option.ConnectionUserName</name>
	    <value>APP</value>
	    <description>Username to use against metastore database</description>
	  </property>
	```
	
	将 value 修改为：
	
	```
	<value>root</value>
	```
		
	(6) 约第 690 行，找到以下属性：
	
	```
	  <property>
	    <name>hive.metastore.schema.verification</name>
	    <value>true</value>
	    <description>
	      Enforce metastore schema version consistency.
	      True: Verify that version information stored in is compatible with one from Hive jars.  Also disable automatic
	            schema migration attempt. Users are required to manually migrate schema after Hive upgrade which ensures
	            proper metastore schema migration. (Default)
	      False: Warn if the version information stored in metastore doesn't match with one from in Hive jars.
	    </description>
	  </property>
	```
	
	将 value 修改为：
	
	```
	<value>false</value>
	```

	(7) 约第936行，找到：
	
	```
	  <property>
	    <name>javax.jdo.option.ConnectionDriverName</name>
	    <value>com.apache.derby.jdbc.EmbeddedDriver</value>
	    <description>Driver class name for a JDBC metastore</description>
	  </property>
	```
	
	将 value 修改为：
	
	```
	<value>org.mysql.jdbc.Driver</value>
	```
	(8) 将所有的 `${system:Java.io.tmpdir}` 替换成 `/home/spark/hive/hive/iotmp` 。

	> 全局替换命令的用法：在 vim 编辑器中，先按 `Esc` 键，再同时按下 `shift` + `:` 键，将以下替换命令粘贴后按回车即可全局替换。
	
	```
	%s#${system:java.io.tmpdir}#/home/spark/hive/iotmp#g
	```

	(9) 将所有的 `${system:user.name}` 替换成 `${user.name}` 。

	```
	%s#${system:user.name}#${user.name}#g
	```
7. 为上一步的步骤 (8) 中的 `${system:java.io.tmpdir}` 目录的替换值创建对应的目录，例如 `/home/spark/hive/hive/iotmp`
	```
	mkdir /home/spark/hive/iotmp  
	chmod 777 /home/spark/hive/hive/iotmp  
	```
8. 编辑 hive-env.sh ，在文件中添加以下内容：
	```
	export JAVA_HOME=/opt/jdk1.8.0_121
	export HADOOP_HOME=/opt/hadoop-2.6.5
	export HIVE_HOME=/opt/apache-hive-2.1.1-bin
	export HIVE_CONF_DIR=/opt/apache-hive-2.1.1-bin/conf
	export HIVE_AUX_JARS_PATH=/opt/apache-hive-2.1.1-bin/lib
	```
9. 复制 MySQL JDBC Driver 到 Hive 的 lib 目录。
	```
	cp /home/spark/Downloads/mysql-connector-java-5.1.41/mysql-connector-java-5.1.41-bin.jar /opt/apache-hive-2.1.1-bin/lib/
	```
10. 初始化 MySQL Schema 。
	```
	schematool -initSchema -dbType mysql
	```
	> 此步骤如果遇到错误，请参考 7.2 小节。

### 8.2 可能遇到的问题

在步骤 10 中，可能会出现以下问题。下面提供了一些解决方法。

#### 8.2.1 数据库访问权限错误

如果出现错误：

```
ERROR 1045 (28000): Access denied for user 'hive'@'localhost' (using password: YES)
```

解决办法：

删除数据库中的空白账户（匿名账户），**在 MySQL 中**依次执行以下两条指命。

> 注意 MySQL 用户的权限。

```
DELETE FROM `mysql`.`user` WHERE `user`='';
FLUSH PRIVILEGES;
```
	
#### 8.2.2 键名错误

如果出现错误：

```
Error: Duplicate key name 'PCS_STATS_IDX' (state=42000,code=1061)
```

解决办法：删除 MySQL 数据库中的 hive 数据库。

进入 MySQL 后输入：

```
DROP DATABASE hive;
```
#### 8.2.3 HDFS 无法访问错误

如果出现错误：

```
Exception in thread "main" java.lang.RuntimeException: org.apache.hadoop.ipc.RemoteException(org.apache.hadoop.hdfs.server.namenode.SafeModeException): Cannot create directory /tmp/hive/spark/48cf67f3-905b-4e17-b864-23ed3dac2e6a. Name node is in safe mode.
```

解决办法：

关闭HDFS的安全模式。

```
hadoop dfsadmin -safemode leave
```

### 8.3 启动和关闭 Hive 

输入 `hive` 并回车，即可启动 Hive 的命令行。

如果需要退出命令行，则在命令行中输入 `exit;` 即可。

Hive 的具体使用方法请参考官方文档。

## 九、安装和配置 Sqoop

### 9.1 配置 Sqoop 

以 Sqoop 1.4.6 版本为例，下载和解压命令如下：

```
cd ~/Downloads
wget https://mirrors.tuna.tsinghua.edu.cn/apache/sqoop/1.4.6/sqoop-1.4.6.bin__hadoop-2.0.4-alpha.tar.gz
tar zxvf sqoop-1.4.6.bin__hadoop-2.0.4-alpha.tar.gz
sudo mv sqoop-1.4.6.bin__hadoop-2.0.4-alpha.tar.gz /opt/sqoop-1.4.6
chown -R spark /opt/sqoop-1.4.6/
```

编辑 `/etc/profile` 文件，添加以下内容：

```
# Sqoop HOME
export SQOOP_HOME=/opt/sqoop-1.4.6
export PATH=$PATH:$SQOOP_HOME/bin
```

> 不要忘记 `source /etc/profile` 使其生效。

配置

```
cd /opt/sqoop-1.4.6
cd conf/
mv sqoop-env-template.sh sqoop-env.sh
vim sqoop-env.sh
```

### 9.2 配置数据库驱动

#### 9.2.1 配置 MySQL JDBC Driver

通过以下命令来获取和解压 MySQL JDBC Driver ，并将 jar 包复制至 Sqoop 的 lib 目录。

```
cd ~/Downloads/
wget https://dev.mysql.com/get/Downloads/Connector-J/mysql-connector-java-5.1.41.tar.gz
tar zxvf mysql-connector-java-5.1.41.tar.gz
cd mysql-connector-java-5.1.41
cp mysql-connector-java-5.1.41-bin.jar /opt/sqoop-1.4.6/lib/
```

#### 9.2.2 配置 SQLServer JDBC Driver


> 下载地址可以在 https://www.microsoft.com/zh-CN/download/details.aspx?id=21599 获取。

```
cd ~/Downloads/
wget https://download.microsoft.com/download/3/D/4/3D4DBEAB-B3D2-41DF-87BC-0FCA33CAAB5C/2052/sqljdbc_3.0.1301.101_chs.tar.gz
tar zxvf sqljdbc_3.0.1301.101_chs.tar.gz
cd sqljdbc_3.0/chs
cp sqljdbc4.jar /opt/sqoop-1.4.6/lib/
```

#### 9.3 测试连接和导入数据

> 注意：不同的数据库可能有不同的 JDBC URL 写法，请根据实际情况参照相应文档编写访问数据库的 JDBC URL 。

测试连接：

```
sqoop list-databases --connect jdbc:mysql://spark1:3306/test --username root -P
```

测试导入：

> 前提是已开放对应端口，有对应的数据库和表，并且记录中含有主键。

从 MySQL 中测试导入数据。

```
sqoop import --hive-import --connect jdbc:mysql://spark1:3306/test --username root  --password root --table student --hive-table mysqltesttable
```

从 SQL Server 中测试导入数据。

```
sqoop import --hive-import --connect 'jdbc:sqlserver://IP地址或hostname或URL:端口号;username=用户名;password=密码;DatabaseName=数据库名' --table property --hive-table sqlservertesttable --where "PropertyID = '17050217465260A4F907CD784C8CE75D'"
```

> 此处导入的参数仅供参考，请详细阅读 Sqoop 官方文档后再设置相应参数。

如果导入成功，在 hive 默认的 default 数据库中可以查看到以上导入的记录。

```
hive> SHOW DATABASES;
hive> USE default;
hive> SHOW TABLES;
hive> SELECT * FROM mysqltesttable;
hive> SELECT * FROM sqlservertesttable;
```

#### 9.4 注意事项

- 在生产环境中，请为不同的数据库访问对象创建相应的用户，并设置合适的权限。
- 在生产环境中，请考虑为数据库访问设置 SSL 加密连接。